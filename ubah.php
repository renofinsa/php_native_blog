<?php 
    include('config/koneksi.php');

    //melihat data berdasarkan id
    $terima = $_GET["id"];
    $data = mysqli_query($con, "SELECT * FROM blog WHERE id = '$terima' ");
    $a = mysqli_fetch_assoc($data);
    $id = $a['id'];
    $judul = $a['judul'];
    $isi = $a['isi'];
    $gambar = $a['gambar'];
    $tanggal = $a['tanggal_buat'];
    //melihat data berdasarkan id



    // coding php untuk merubah data 
    if(isset($_POST['simpan'])){
        
        // source merubah gambar
         $cv = $_POST['x'];
         $foto        =$_FILES['gambar']['tmp_name'];
         $foto_name   =$_FILES['gambar']['name'];
         $acak        = rand(100,100000);
         $tujuan_foto = $acak.$foto_name;
         $tempat_foto = 'public/images/'.$tujuan_foto;
 
         if(!$foto == ""){
           $buat_foto=$tujuan_foto;
           $f = 'public/images/'.$cv;
           @unlink ("$f");
           copy ($foto,$tempat_foto);
         }else{
           $buat_foto=$cv;
         }
         // end source merubah gambar

        $judul = $_POST['judul'];
        $isi = $_POST['isi'];
        $tanggal = $_POST['tanggal'];

        $sql = mysqli_query($con, "UPDATE blog SET
        judul = '$judul',
        gambar = '$buat_foto',
        isi = '$isi',
        tanggal_buat = '$tanggal'
        WHERE id = '$id' ");
        if ($sql) {
            echo '<script language="javascript">alert("Berhasil Menyimpan"); document.location="index.php";</script>'; 
          }else{
            echo '<script language="javascript">alert("Coba lagi !!!");document.location="index.php";</script>';
          }

    }

    // coding php untuk merubah data 

?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ubah -> <?php echo $judul ?></title>
    <!-- head css -->
    <?php include('public/components/head.php') ?>
    <!-- head css -->
  </head>
  <body>
    <!-- navibar website -->
        <?php include('public/components/nav.php') ?>
    <!-- navibar website -->
    <div class="container">
        <h4>Ubah Data - <?php echo $judul ?></h4>
        <div class="row margin">
            <div class="col-md-6 col-md-offset-3">
                <div class="thumbnail">
                    <div class="caption">
                        <form action="" method="POST" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $id ?>" name="id">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <img style="height: 200px; width: 200px;" src="public/images/<?php echo $gambar ?>">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="">Gambar</label>
                                        <input type="file" name="gambar" class="form-control">
                                        <input type="hidden" name="x" class="form-control" value="<?php echo $gambar ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Judul (Max: 50 character)</label>
                                        <input value="<?php echo $judul ?>" type="text" name="judul" class="form-control" placeholder="belajar php native" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tanggal Buat (M/D/Y)</label>
                                        <input value="<?php echo $tanggal ?>" type="date" name="tanggal" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Isi</label>
                                <textarea class="form-control" name="isi" col="10" rows="10" placeholder="write here ..."><?php echo $isi ?></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6 padding">
                                    <button name="simpan" type="submit" class="btn btn-primary form-control">Simpan</button>
                                </div>
                                <div class="col-md-6 padding">
                                    <a href="index.php" class="btn btn-danger form-control">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- footer -->
    <?php include('public/components/footer.php') ?>
    <!-- footer -->

    
    <!-- js script -->
    <?php include('public/components/script.php') ?>
    <!-- js script -->
  </body>
</html>