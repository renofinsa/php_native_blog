-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 20, 2018 at 06:25 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tutorial_php_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `gambar` text,
  `tanggal_buat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `judul`, `isi`, `gambar`, `tanggal_buat`) VALUES
(8, 'Belajar HTML 5 ', 'HTML5 adalah sebuah bahasa markah untuk menstrukturkan dan menampilkan isi dari Waring Wera Wanua, sebuah teknologi inti dari Internet. HTML5 adalah revisi kelima dari HTML (yang pertama kali diciptakan pada tahun 1990 dan versi keempatnya, HTML4, pada tahun 1997) dan hingga bulan Juni 2011 masih dalam pengembangan.', '17384-html.jpg', '2018-09-20'),
(9, 'Belajar CSS 3', 'CSS 3 adalah versi CSS terbaru yang masih dikembangkan oleh W3C. Namun beberapa web browser sudah mendukung CSS 3. CSS 2 didukung seutuhnya oleh CSS 3 dan tidak ada perubahan, hanya ada beberapa penambahan, sehingga ketika bermigrasi dari CSS 2 ke CSS 3, tidak perlu mengubah apapun.', '90706-css.jpg', '2018-09-20'),
(10, 'Belajar Bootstrap 3', 'Bootstrap adalah sebuah framework css yang dapat digunakan untuk mempermudah membangun tampilan web. Bootstrap pertama kali di kembangkan pada pertangahan 2010 di Twitter oleh Mark Otto dan Jacob Thornton. Saat ini Bootstrap dikembangkan secara open source dengan lisensi MIT.', '80729-bootstrap.jpg', '2018-09-20'),
(11, 'Belajar Database dengan MySQL', 'MySQL adalah sebuah perangkat lunak system manajemen basis data SQL (DBMS) yang multithread, dan multi-user. MySQL adalah implementasi dari system manajemen basisdata relasional (RDBMS).MySQL AB merupakan perusahaan komersial Swedia yang mensponsori dan yang memiliki MySQL', '36087-mysql.jpg', '2018-09-20'),
(12, 'Belajar PHP Native', 'PHP adalah singkatan dari \"PHP: Hypertext Prepocessor\", yaitu bahasa pemrograman yang digunakan secara luas untuk penanganan pembuatan dan pengembangan sebuah situs web dan bisa digunakan bersamaan dengan HTML. PHP diciptakan oleh Rasmus Lerdorf pertama kali tahun 1994.', '45522-php.jpg', '2018-09-20'),
(13, 'Mengenal Founder PHP', 'Rasmus Lerdorf (dilahirkan pada tanggal 22 November 1968 di Qeqertarsuaq, Greenland) merupakan seorang pemrogram dari Denmark/Greenland dan pencipta bahasa pemrograman PHP. Rasmus menulis dua versi pertama dari PHP. Rasmus juga berpartisipasi dalam pengembangan versi-versi PHP selanjutnya yang dikembangkan oleh sebuah kelompok pengembang, termasuk di dalamnya Andi Gutmans dan Zeev Suraski yang selanjutnya mendirikan Zend Technologies. Pada tahun 1993 Rasmus lulus dari Universitas Waterloo. Sejak bulan September 2002, dia bekerja di Yahoo! sebagai seorang Insinyur Arsitek Infrastruktur.', '23830-rasmus lerdorf.jpg', '2018-09-20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
