<?php 
    include('config/koneksi.php');

    if(isset($_POST['hapus'])){
        $idpilih = $_POST['id'];
        $gambar = $_POST['gambar'];

        // proses penghapusan gambar dari directory / folder
        if($gambar){
            $f = 'public/images/'.$gambar;
            @unlink ("$f");
          }
        // proses penghapusan gambar dari directory / folder
  
        $sql = mysqli_query($con, "DELETE FROM blog WHERE id = '$idpilih'");
  
        if ($sql) {
            echo '<script language="javascript">alert("Blog berhasil dihapus"); document.location="index.php";</script>'; 
        }else{
           echo '<script language="javascript">alert("Gagal menghapus"); document.location="index.php";</script>';
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>PHP Native -> Blog</title>
    <!-- head css -->
    <?php include('public/components/head.php') ?>
    <!-- head css -->
  </head>
  <body>
    <!-- navibar website -->
        <?php include('public/components/nav.php') ?>
    <!-- navibar website -->
    <div class="container">
        <div class="row margin">
            <?php 
                $sql = mysqli_query($con, "SELECT * FROM blog ORDER BY id DESC");
                while($a = mysqli_fetch_array($sql)){
                    $id = $a['id'];
                    $judul = $a['judul'];
                    $isi = $a['isi'];
                    $gambar = $a['gambar'];
                    $date = date_create($a['tanggal_buat']);
                    $tanggal = date_format($date,"d-M-Y");
            ?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img style="height: 250px" src="public/images/<?php echo $gambar ?>">
                    <div class="caption">
                        <p><b>( <?php echo $tanggal ?> )</b></p>
                        <a href="lihat.php?id=<?php echo $id ?>" ><h4><?php echo $judul ?></h4></a>
                        <hr>
                        <div class="row">
                            <div class="col-md-6 padding">
                                 <a href="ubah.php?id=<?php echo $id ?>" class="btn btn-success form-control" role="button">Ubah</a>
                            </div>
                            <div class="col-md-6 padding">
                                <button type="button" class="btn btn-danger form-control" data-toggle="modal" data-target="#myModal<?php echo $id ?>">Hapus</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <!-- Modal -->
                    <div id="myModal<?php echo $id ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <p>Apa kamu yakin ingin menghapus blog <b><u><?php echo $judul ?></u></b> ?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" value="<?php echo $id ?>" name="id">
                                        <input type="hidden" value="<?php echo $gambar ?>" name="gambar">
                                       <div class="row">
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary form-control" name="hapus">Hapus</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-danger form-control" data-dismiss="modal">Tidak</button>
                                            </div>
                                       </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                <!-- Modal  -->

            <?php } ?>

            
        </div>
    </div>
    <!-- footer -->
    <?php include('public/components/footer.php') ?>
    <!-- footer -->

    
    <!-- js script -->
    <?php include('public/components/script.php') ?>
    <!-- js script -->
  </body>
</html>