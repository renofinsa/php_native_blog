<?php 
    include('config/koneksi.php');


    // coding php untuk memasukan data 
    if(isset($_POST['simpan'])){

        // source input gambar
        $lokasi_image = $_FILES['gambar']['tmp_name'];
        if($_FILES['gambar']['name']==""){
         $cover = NULL;
        }else{
         $cover = rand(100,100000)."-".$_FILES['gambar']['name'];
        }
        $temp="public/images/$cover";
        move_uploaded_file($lokasi_image,$temp);
        // end source input gambar

        $judul = $_POST['judul'];
        $isi = $_POST['isi'];
        $sql = mysqli_query($con, "INSERT INTO blog VALUES (NULL,'$judul','$isi','$cover',NOW() ) ");
        if ($sql) {
            echo '<script language="javascript">alert("Berhasil Menyimpan"); document.location="index.php";</script>'; 
          }else{
            echo '<script language="javascript">alert("Coba lagi !!!");document.location="index.php";</script>';
          }

    }

    // coding php untuk memasukan data 

?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Native -> Create Blog</title>
    <!-- head css -->
    <?php include('public/components/head.php') ?>
    <!-- head css -->
  </head>
  <body>
    <!-- navibar website -->
        <?php include('public/components/nav.php') ?>
    <!-- navibar website -->
    <div class="container">
        <h4>Membuat Blog</h4>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="thumbnail">
                    <div class="caption">
                        <form action="" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="gambar" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Judul (Max: 50 character)</label>
                                <input type="text" name="judul" class="form-control" placeholder="belajar php native" required>
                            </div>
                            <div class="form-group">
                                <label for="">Isi</label>
                                <textarea class="form-control" name="isi" col="10" rows="10" required placeholder="write here ..."></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button name="simpan" type="submit" class="btn btn-primary form-control">Simpan</button>
                                </div>
                                <div class="col-md-6">
                                    <a href="index.php" name="simpan" class="btn btn-danger form-control">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- footer -->
    <?php include('public/components/footer.php') ?>
    <!-- footer -->

    
    <!-- js script -->
    <?php include('public/components/script.php') ?>
    <!-- js script -->
  </body>
</html>