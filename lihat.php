<?php 
    include('config/koneksi.php');

    //melihat data berdasarkan id
    $terima = $_GET["id"];
    $data = mysqli_query($con, "SELECT * FROM blog WHERE id = '$terima' ");
    $a = mysqli_fetch_assoc($data);
    $id = $a['id'];
    $judul = $a['judul'];
    $isi = $a['isi'];
    $gambar = $a['gambar'];
    $date = date_create($a['tanggal_buat']);
    $tanggal = date_format($date,"d-M-Y");
    //melihat data berdasarkan id

?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $judul ?></title>
    <!-- head css -->
    <?php include('public/components/head.php') ?>
    <!-- head css -->
  </head>
  <body>
    <!-- navibar website -->
        <?php include('public/components/nav.php') ?>
    <!-- navibar website -->
    <div class="container">
        <div class="row margin">
            <div class="col-md-6 col-md-offset-3">
                <div class="thumbnail">
                    <div class="caption">
                        

                        <div class="row">
                             <div class="col-md-12">
                                <div class="form-group">
                                    <img style="height: 50%; width: 100%;" src="public/images/<?php echo $gambar ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <h3><?php echo $judul ?></h3>
                            </div>
                            <div class="col-md-4">
                                <p style="text-align:right"><?php echo $tanggal ?></p>
                            </div>
                        </div>
                        <hr>
                        <p><?php echo $isi ?></p>
                        <div class="form-group margin">
                            <a href="index.php" class="btn btn-primary form-control">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- footer -->
    <?php include('public/components/footer.php') ?>
    <!-- footer -->

    
    <!-- js script -->
    <?php include('public/components/script.php') ?>
    <!-- js script -->
  </body>
</html>